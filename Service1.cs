﻿using Microsoft.AspNet.SignalR.Client;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace has.iot.smartwatch.service
{
    public partial class Service1 : ServiceBase
    {
        #region variable
        private static MqttClient MqttClient_1 = null;
        private static MqttClient MqttClient_2 = null;
        private static string MQTTTopic_1;
        private static string MQTTTopic_2;
        private static MongoClient client = null;
        private static IMongoDatabase database = null;
        private static IMongoDatabase databaseHM = null;
        private int timesoff = 25330;
        private string clientId_1;
        static HubConnection connection = null;
        static IHubProxy appHub = null;
        static string hubUrl;
        #endregion



        public Service1()
        {
            InitializeComponent();
        }
        public void OnDebug()
        {
            OnStart(null);
        }
        protected override void OnStart(string[] args)
        {
            try
            {
                string signalRsvr = ConfigurationManager.AppSettings["signalr_server"].ToString();
                string mongoDBsvr = ConfigurationManager.AppSettings["mongodb_server"].ToString();
                string MQTTBroker_1 = ConfigurationManager.AppSettings["MQTTBroker_1"].ToString();
                string MQTTBroker_2 = ConfigurationManager.AppSettings["MQTTBroker_2"].ToString();
                MQTTTopic_1 = ConfigurationManager.AppSettings["MQTTTopic_1"].ToString();
                MQTTTopic_2 = ConfigurationManager.AppSettings["MQTTTopic_2"].ToString();
                string MQTTBrokerMultiple = ConfigurationManager.AppSettings["MQTTBrokerMultiple"].ToString();
                string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
                if (File.Exists(basepatch))
                {
                    string Json = File.ReadAllText(basepatch);
                    if (Json != "null")
                    {
                        object rootresult = JsonConvert.DeserializeObject(Json);
                        JObject rootobj = JObject.Parse(rootresult.ToString());
                        signalRsvr = rootobj["signalr_server"].ToString();
                        mongoDBsvr = rootobj["mongodb_server"].ToString();
                        MQTTBroker_1 = rootobj["MQTTBroker_1"].ToString();
                        MQTTBroker_2 = rootobj["MQTTBroker_2"].ToString();
                        MQTTTopic_1 = rootobj["MQTTTopic_1"].ToString();
                        MQTTTopic_2 = rootobj["MQTTTopic_2"].ToString();
                        MQTTBrokerMultiple = rootobj["MQTTBrokerMultiple"].ToString();
                        timesoff = Convert.ToInt32(rootobj["timesoff"].ToString());
                        hubUrl = signalRsvr;
                    }
                }

                // mongoDB connection 
                client = new MongoClient(mongoDBsvr);
                database = client.GetDatabase("IoT");
                databaseHM = client.GetDatabase("HumanMonitoring");

                if (MQTTBrokerMultiple == "true")
                {
                    // mqtt connection device
                    MqttClient_1 = new MqttClient(MQTTBroker_1);
                    MqttClient_1.MqttMsgPublishReceived += client_MqttMsgPublishReceived_1;
                    MqttClient_1.Subscribe(new string[] { MQTTTopic_1 + "#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                    clientId_1 = Guid.NewGuid().ToString();
                    MqttClient_1.Connect(clientId_1);

                    void client_MqttMsgPublishReceived_1(object sender, MqttMsgPublishEventArgs e)
                    {
                        try
                        {
                            var message = System.Text.Encoding.Default.GetString(e.Message);
                            if (IsValidJson(message))
                            {
                                object result = JsonConvert.DeserializeObject(message);
                                JObject voobj = JObject.Parse(result.ToString());

                                string[] topic = e.Topic.Split('/');
                                if (topic.Length > 1)
                                {
                                    //writeTxt("data collected : " + voobj, "raw_data");
                                    LogHandler(voobj, topic);
                                    LogHandlerHumanMonitoring(voobj, topic);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            writeLog(ex.ToString(), Constants.SW_ERROR_CODE);
                        }

                    }
                }

                // mqtt connection apps
                MqttClient_2 = new MqttClient(MQTTBroker_2);
                MqttClient_2.MqttMsgPublishReceived += client_MqttMsgPublishReceived_2;
                MqttClient_2.Subscribe(new string[] { MQTTTopic_2 + "#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                string clientId_2 = Guid.NewGuid().ToString();
                MqttClient_2.Connect(clientId_2);

                void client_MqttMsgPublishReceived_2(object sender, MqttMsgPublishEventArgs e)
                {
                    //string[] topic = e.Topic.Split('/');
                    //var message = System.Text.Encoding.Default.GetString(e.Message);
                    //if (IsValidJson(message))
                    //{
                    //    object result = JsonConvert.DeserializeObject(message);
                    //    JObject voobj = JObject.Parse(result.ToString());

                    //    if (topic.Length > 1)
                    //    {
                    //        LogHandler(voobj, topic);
                    //    }
                    //}
                    //else
                    //{
                    //    if (topic.Length > 1 && MQTTBrokerMultiple == "true")
                    //    {
                    //        //string cmd = topic[3];
                    //        //switch (cmd)
                    //        //{
                    //        //    case "cmnd":
                    //        //        MqttClient_1.Publish(e.Topic, e.Message, MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                    //        //        break;
                    //        //    default:

                    //        //        break;
                    //        //}
                    //    }
                    //}
                }

            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        public static void writeLog(string log, int eventId)
        {
            try
            {
                string instance = "WINSVC_SMARTWATCH";
                string newLog = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "]" + instance + log;
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry(newLog, EventLogEntryType.Error, eventId, 1);
                }
            }
            catch (Exception ex)
            {

            }
        }
        private async Task TryReconnectAsync(CancellationToken cancellationToken)
        {
            var connected = MqttClient_1.IsConnected;
            while (!connected && !cancellationToken.IsCancellationRequested)
            {
                try
                {
                    MqttClient_1.Connect(clientId_1);
                }
                catch
                {

                }
                connected = MqttClient_1.IsConnected;
                await Task.Delay(10000, cancellationToken);
            }
        }
        public static void LogHandler(JObject voobj, string[] topic)
        {
            #region set Connect to Database
            var DataLogs = database.GetCollection<BLESmartWatchData>("SmartwatchDataLogs");
            var collection = database.GetCollection<BLESmartWatchDataView>("SmartwatchDataLogs");
            var SWgateway = database.GetCollection<SmartWatchGatewayView>("SmartwatchGateway");
            var Person = database.GetCollection<PersonView>("Person");
            var PersonTags = database.GetCollection<PersonTagsView>("PersonTags");
            var PersonLoc = database.GetCollection<PersonLocation>("PersonLocation");
            var SmartwatchPersonZone = database.GetCollection<SmartwatchPersonZone>("SmartwatchPersonZone");
            var constantVal = database.GetCollection<ConfigurationModel>("Configuration");
            string device_active = ConfigurationManager.AppSettings["device_active"].ToString();

            //SmartWatch Data Room
            var smartWatchDataRoom = database.GetCollection<SmartWatchDataRoom>("SmartWatchDataRoom");
            var smartWatchDataRoomDetail = database.GetCollection<SmartWatchDataRoomDetail>("SmartWatchDataRoomDetail");
            var PersonStatus = database.GetCollection<PersonStatusDatabase>("PersonStatus");

            //Avatar Location
            var AvatarLocation = database.GetCollection<AvatarLocation>("AvatarLocation");
            #endregion

            #region write text raw data
            string newLogStart = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + "data raw : " + voobj;
            StringBuilder sb = new StringBuilder();
            sb.Append(newLogStart + Environment.NewLine);
            File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "raw_tag_" + DateTime.Now.ToString("yyyyMMdd") + ".txt", sb.ToString());
            sb.Clear();
            #endregion

            #region loging and checking gadget usage
            if (voobj.ContainsKey("dt"))
            {
                object resultEnergi = JsonConvert.DeserializeObject(voobj["dt"].ToString());
                JObject obj = JObject.Parse(resultEnergi.ToString());

                string mac = (obj["mac"] == null) ? "0" : obj["mac"].ToString().ToString();
                string type = (obj["type"] == null) ? "0" : obj["type"].ToString();
                string battery = (obj["battery"] == null) ? "0" : obj["battery"].ToString();
                string heartrate = (obj["heartrate"] == null) ? "0" : obj["heartrate"].ToString();
                string step = (obj["step"] == null) ? "0" : obj["step"].ToString();
                string sleep = (obj["sleep"] == null) ? "0" : obj["sleep"].ToString();
                string calories = (obj["calories"] == null) ? "0" : obj["calories"].ToString();
                string blood = (obj["blood"] == null) ? "0" : obj["blood"].ToString();
                string temperature = (obj["temperature"] == null) ? "0" : obj["temperature"].ToString();
                string sos = (obj["sos"] == null) ? "0" : obj["sos"].ToString();
                string unwear = (obj["unwear"] == null) ? "0" : obj["unwear"].ToString();
                string falling = (obj["falling"] == null) ? "0" : obj["falling"].ToString();
                string nearby = (obj["nearby"] == null) ? "0" : obj["nearby"].ToString();
                string battery_update = (obj["battery_update"] == null) ? "0" : obj["battery_update"].ToString();
                string blood_update = (obj["blood_update"] == null) ? "0" : obj["blood_update"].ToString();
                string temperature_update = (obj["temperature_update"] == null) ? "0" : obj["temperature_update"].ToString();

                BLESmartWatchData voData = new BLESmartWatchData();

                var GWbuilder = Builders<SmartWatchGatewayView>.Filter;
                var GWflt = GWbuilder.Eq<string>("gatewayID", nearby);
                var GWres = SWgateway.Find(GWflt).SingleOrDefault();
                if (GWres != null)
                {
                    voData.location = GWres.location;
                }

                var PTbuilder = Builders<PersonTagsView>.Filter;
                var PTflt = PTbuilder.And(PTbuilder.Eq<string>("type", "SW"), PTbuilder.Eq<string>("tagID", mac));
                var PTres = PersonTags.Find(PTflt).FirstOrDefault();
                if (PTres != null)
                {
                    var Pbuilder = Builders<PersonView>.Filter;
                    var Pflt = Pbuilder.Eq<string>("personID", PTres.personID);
                    var PWres = Person.Find(Pflt).FirstOrDefault();
                    if (PWres != null)
                    {
                        // loging
                        voData.person = PTres.personID;
                        voData.RecordTimestamp = DateTime.Now;
                        voData.mac = mac;
                        voData.type = type;
                        voData.battery = Convert.ToInt32(battery);
                        voData.heartrate = Convert.ToInt32(heartrate);
                        voData.step = Convert.ToInt32(step);
                        voData.sleep = Convert.ToInt32(sleep);
                        voData.calories = Convert.ToInt32(calories);
                        voData.blood = Convert.ToInt32(blood);
                        voData.temperature = Convert.ToDecimal(temperature);
                        //voData.sos = Convert.ToInt32(sos);
                        //voData.unwear = Convert.ToInt32(unwear);
                        voData.sos = Convert.ToBoolean(sos);
                        voData.unwear = Convert.ToBoolean(unwear);
                        voData.falling = Convert.ToBoolean(falling);
                        voData.nearby = Convert.ToInt32(nearby);
                        voData.battery_update = Convert.ToBoolean(battery_update);
                        voData.blood_update = Convert.ToBoolean(blood_update);
                        voData.temperature_update = Convert.ToBoolean(temperature_update);
                        DataLogs.InsertOne(voData);

                        //Avatar Location
                        var avatarID = PWres.avatarID;
                        var defaultZone = PWres.defaultZone;
                        var dataAvatarNamePerson = "";
                        var dataAvatarLocation = "";
                        if (voData.location != defaultZone)
                        {
                            //Avatar Location
                            var ALbuilder = Builders<AvatarLocation>.Filter;
                            var ALflt = ALbuilder.Eq<string>("location", voData.location);
                            var ALres = AvatarLocation.Find(ALflt).ToList();
                            foreach (AvatarLocation ALData in ALres)
                            {
                                //check avatar user
                                var avatarCheck = Pbuilder.Eq<string>("avatarID", ALData.avatarName);
                                var avatarIDLocation = Person.Find(avatarCheck).Limit(1).SortByDescending(f => f.RecordTimestamp).FirstOrDefault();
                                if (avatarIDLocation == null)
                                {
                                    dataAvatarNamePerson = ALData.avatarName;
                                    dataAvatarLocation = ALData.location;
                                }
                            }

                            var updatePerson = Builders<PersonView>.Update.Set("RecordTimestamp", DateTime.Now)
                                .Set("defaultZone", dataAvatarLocation)
                                .Set("avatarID", dataAvatarNamePerson);
                            Person.UpdateOne(Pflt, updatePerson);

                            defaultZone = dataAvatarLocation;
                            avatarID = dataAvatarNamePerson;
                        }

                        // person location
                        PersonLocation dataTG = new PersonLocation();
                        var TGbuilder = Builders<PersonLocation>.Filter;
                        var TGflt = TGbuilder.Eq<string>("personID", PTres.personID);
                        var TGWres = PersonLoc.Find(TGflt).SingleOrDefault();
                        if (TGWres == null)
                        {
                            dataTG.RecordTimestamp = DateTime.Now;
                            dataTG.location = voData.location;
                            dataTG.personID = PTres.personID;
                            dataTG.personName = PWres.personName;
                            dataTG.rssi = voData.nearby.ToString();
                            dataTG.tagID = voData.mac;
                            dataTG.gatewayID = (GWres != null) ? GWres.gatewayName : "";
                            dataTG.type = "SW";
                            dataTG.defaultZone = defaultZone;
                            dataTG.avatarID = avatarID;
                            PersonLoc.InsertOne(dataTG);
                        }
                        else
                        {
                            var update = Builders<PersonLocation>.Update.Set("RecordTimestamp", DateTime.Now)
                            .Set("personID", PTres.personID)
                            .Set("rssi", voData.nearby.ToString())
                            .Set("type", "SW")
                            .Set("location", voData.location)
                            .Set("avatarID", avatarID)
                            .Set("gatewayID", (GWres != null) ? GWres.gatewayName : "");
                            PersonLoc.UpdateOne(TGflt, update);
                        }

                        // Smart watch person zone
                        SmartwatchPersonZone dataZone = new SmartwatchPersonZone();
                        var Zonebuilder = Builders<SmartwatchPersonZone>.Filter;
                        var Zoneflt = Zonebuilder.Eq<string>("tagID", "");
                         Zoneflt &= Zonebuilder.Eq<string>("location", voData.location);
                        var Zonefltres = SmartwatchPersonZone.Find(Zoneflt).FirstOrDefault();
                        if(Zonefltres != null)
                        {
                            if (voData.location != null)
                            {
                                var Zonebuilder2 = Builders<SmartwatchPersonZone>.Filter;
                                var Zoneflt2 = Zonebuilder2.Eq<string>("tagID", voData.mac);
                                var ZoneWres2 = SmartwatchPersonZone.Find(Zoneflt2).FirstOrDefault();
                                if (ZoneWres2 != null)
                                {
                                    var updateZone2 = Builders<SmartwatchPersonZone>.Update.Set("RecordTimestamp", DateTime.Now)
                                    .Set("RecordTimestamp", DateTime.Now)
                                    .Set("tagID", "")
                                    .Set("rssi", "")
                                    .Set("personID", "")
                                    .Set("avatarID", "")
                                    .Set("personName", "");
                                    SmartwatchPersonZone.UpdateOne(Zoneflt2, updateZone2);
                                }

                                var updateZone = Builders<SmartwatchPersonZone>.Update.Set("RecordTimestamp", DateTime.Now)
                                .Set("personID", PTres.personID)
                                .Set("tagID", voData.mac)
                                .Set("rssi", voData.nearby.ToString())
                                .Set("avatarID", avatarID)
                                .Set("LastSeen", DateTime.Now);
                                SmartwatchPersonZone.UpdateOne(Zoneflt, updateZone);
                            }                                
                        }

                        //check treshold
                        if (voData.mac == device_active)
                        {
                            var filter = new BsonDocument() { };
                            ConfigurationModel resConsVal = constantVal.Find(filter).Limit(1).SingleOrDefault();
                            if (resConsVal != null)
                            {
                                if (voData.temperature > resConsVal.SkinTemperature && resConsVal.SkinTemperatureStatus == true)
                                {
                                    object msgCoveLight = new
                                    {
                                        deviceID = "127001",
                                        ObjectType = "OBJECT_BINARY_VALUE:28",
                                        value = "1",
                                    };
                                    object msgSurroundLight12 = new
                                    {
                                        deviceID = "127001",
                                        ObjectType = "OBJECT_BINARY_VALUE:1799",
                                        value = "1",
                                    };
                                    MqttClient_2.Publish("Curtain/Bacnet/Write", Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(msgCoveLight)), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                    MqttClient_2.Publish("Curtain/Bacnet/Write", Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(msgSurroundLight12)), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                }
                                else if (voData.heartrate > resConsVal.HeartRate && resConsVal.HeartRateStatus == true)
                                {
                                    object msgCoveLight = new
                                    {
                                        deviceID = "127001",
                                        ObjectType = "OBJECT_BINARY_VALUE:28",
                                        value = "1",
                                    };
                                    object msgSurroundLight12 = new
                                    {
                                        deviceID = "127001",
                                        ObjectType = "OBJECT_BINARY_VALUE:1799",
                                        value = "1",
                                    };
                                    MqttClient_2.Publish("Curtain/Bacnet/Write", Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(msgCoveLight)), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                    MqttClient_2.Publish("Curtain/Bacnet/Write", Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(msgSurroundLight12)), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                                }
                            }
                        }

                        if (voData.location != null)
                        {
                            // SmartWatch Data Room
                            SmartWatchDataRoom DataRoom = new SmartWatchDataRoom();
                            SmartWatchDataRoomDetail DataRoomDetail = new SmartWatchDataRoomDetail();
                            PersonStatusDatabase DataPersonStatus = new PersonStatusDatabase();
                            var filter = new BsonDocument() { };
                            ConfigurationModel resConsVal = constantVal.Find(filter).Limit(1).SingleOrDefault();
                            var SWDRoom = Builders<SmartWatchDataRoom>.Filter;
                            var SWDRoomDetail = Builders<SmartWatchDataRoomDetail>.Filter;
                            var PSBld = Builders<PersonStatusDatabase>.Filter;
                            var SmartWatchDataLog = Builders<BLESmartWatchData>.Filter;

                            // Check Status Person
                            var PersonStatusFlt = PSBld.Eq<string>("personID", PTres.personID);
                            var PersonStatusData = PersonStatus.Find(PersonStatusFlt).Limit(1).SortByDescending(x => x.RecordTimestamp).FirstOrDefault();
                            DataPersonStatus.status = "NEGATIVE";
                            if (PersonStatusData != null)
                            {
                                DataPersonStatus.status = PersonStatusData.status;
                            }

                            // SmartWatch Data Room Detail
                            var SWDRoomDetailFlt = SWDRoomDetail.Eq<string>("location", voData.location);
                            SWDRoomDetailFlt &= SWDRoomDetail.Eq<string>("personID", PTres.personID);
                            var dataPersonLocation = smartWatchDataRoomDetail.Find(SWDRoomDetailFlt).Limit(1).SortByDescending(x => x.RecordTimestamp).FirstOrDefault();
                            if (dataPersonLocation == null)
                            {
                                DataRoomDetail.RecordTimestamp = DateTime.Now;
                                DataRoomDetail.personID = PTres.personID;
                                DataRoomDetail.personName = PWres.personName;
                                DataRoomDetail.location = voData.location;
                                DataRoomDetail.status = DataPersonStatus.status;
                                DataRoomDetail.avatarID = avatarID;
                                smartWatchDataRoomDetail.InsertOne(DataRoomDetail);
                            }
                            else if(resConsVal.HumanTracingStatus == true)
                            {
                                var SWDRoomDetailCheckFlt = SWDRoomDetail.Eq<string>("personID", PTres.personID);
                                var timeHumanTracing = "-" + resConsVal.HumanTracing;
                                var datetimeMonggoDB = DateTime.Now.AddSeconds(Int64.Parse(timeHumanTracing));
                                SWDRoomDetailCheckFlt &= SWDRoomDetail.Gt<DateTime>("RecordTimestamp", datetimeMonggoDB);
                                
                                //Data Person Location Check
                                var dataPLCheck = smartWatchDataRoomDetail.Find(SWDRoomDetailCheckFlt).Limit(1).SortByDescending(x => x.RecordTimestamp).FirstOrDefault();
                                if (dataPLCheck == null)
                                {
                                    var SWDRoomDetailCheckFltLocation = SWDRoomDetail.Eq<string>("personID", PTres.personID);
                                    var dataPLCheckLocation = smartWatchDataRoomDetail.Find(SWDRoomDetailCheckFltLocation).Limit(1).SortByDescending(x => x.RecordTimestamp).FirstOrDefault();
                                    if (dataPLCheckLocation.location != voData.location)
                                    {
                                        string newLog = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + "data Change Treshold : " + voobj;
                                        StringBuilder sbLog = new StringBuilder();
                                        sbLog.Append(newLog + Environment.NewLine);
                                        File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "Insert_Treshold" + "_" + DateTime.Now.ToString("yyyyMMdd") + ".txt", sbLog.ToString());
                                        sbLog.Clear();

                                        DataRoomDetail.RecordTimestamp = DateTime.Now;
                                        DataRoomDetail.personID = PTres.personID;
                                        DataRoomDetail.personName = PWres.personName;
                                        DataRoomDetail.location = voData.location;
                                        DataRoomDetail.status = DataPersonStatus.status;
                                        DataRoomDetail.avatarID = avatarID;
                                        smartWatchDataRoomDetail.InsertOne(DataRoomDetail);
                                    }
                                }
                            }
                            else
                            {
                                var SWDRoomDetailCheckFlt = SWDRoomDetail.Eq<string>("personID", PTres.personID);
                                
                                //Data Person Location Check
                                var dataPLCheck = smartWatchDataRoomDetail.Find(SWDRoomDetailCheckFlt).Limit(1).SortByDescending(x => x.RecordTimestamp).FirstOrDefault();
                                if (dataPLCheck != null)
                                {
                                    if (dataPLCheck.location != voData.location)
                                    {

                                        DataRoomDetail.RecordTimestamp = DateTime.Now;
                                        DataRoomDetail.personID = PTres.personID;
                                        DataRoomDetail.personName = PWres.personName;
                                        DataRoomDetail.location = voData.location;
                                        DataRoomDetail.status = DataPersonStatus.status;
                                        DataRoomDetail.avatarID = avatarID;
                                        smartWatchDataRoomDetail.InsertOne(DataRoomDetail);
                                    }
                                }
                            }
                        }
                    }
                }

                SmartwatchPersonZone dataZone2 = new SmartwatchPersonZone();
                var Zonebuilder3 = Builders<SmartwatchPersonZone>.Filter;
                var Zoneflt3 = Zonebuilder3.Eq<string>(mac, voData.mac);
                var ZoneWres3 = SmartwatchPersonZone.Find(Zoneflt3).FirstOrDefault();
                if (ZoneWres3 != null)
                {
                    if (mac == voData.mac)
                    {
                        var startZone = DateTime.UtcNow;
                        var oldDate = ZoneWres3.LastSeen.AddMinutes(10);
                        if (startZone >= oldDate)
                        {
                            //1 minutes were passed from start 
                            var updateZone = Builders<SmartwatchPersonZone>.Update.Set("RecordTimestamp", DateTime.Now)
                            .Set("tagID", "")
                            .Set("personID", "")
                            .Set("avatarID", "")
                            .Set("personName", "")
                            .Set("rssi", "");
                            SmartwatchPersonZone.UpdateOne(Zoneflt3, updateZone);
                        }
                    }
                }

                // Remove Wearable that out from gateway
                Dictionary<string, string> ResultData = new Dictionary<string, string>();
                var client = new MongoClient();
                var database = client.GetDatabase("IoT");
                var collect = database.GetCollection<SmartwatchPersonZone>("SmartwatchPersonZone");
                List<SmartwatchPersonZone> res = collect.Find(_ => true).ToList();

                ResultData = new Dictionary<string, string>();
                foreach (SmartwatchPersonZone data in res)
                {
                    var startZone = DateTime.UtcNow;
                    var oldDate = data.LastSeen.AddMinutes(5);
                    var id = data._id;
                    var filt = Builders<SmartwatchPersonZone>.Filter.Eq("_id", id);
                    if (startZone >= oldDate)
                    {
                        //10 minutes were passed from start 
                        var updateZone = Builders<SmartwatchPersonZone>.Update.Set("RecordTimestamp", DateTime.Now)
                        .Set("tagID", "")
                        .Set("personID", "")
                        .Set("avatarID", "")
                        .Set("personName", "")
                        .Set("rssi", "");
                        SmartwatchPersonZone.UpdateOne(filt, updateZone);
                    }
                }
            }
            #endregion
        }

        public static void LogHandlerHumanMonitoring(JObject voobj, string[] topic)
        {
            var DataLogs = databaseHM.GetCollection<BLESmartWatchData>("SmartwatchDataLogs");
            var SWgateway = databaseHM.GetCollection<SmartWatchGatewayView>("SmartwatchGateway");
            var Person = databaseHM.GetCollection<PersonView>("Person");
            var PersonTags = databaseHM.GetCollection<PersonTagsView>("PersonTags");
            var PersonLoc = databaseHM.GetCollection<PersonLocation>("PersonLocation");

            //Avatar Location
            var AvatarLocation = databaseHM.GetCollection<AvatarLocation>("AvatarLocation");

            //Event Log Monitoring
            var EventsLog = databaseHM.GetCollection<EventsLogMonitoring>("EventsLog");
            var HumanConfiguration = databaseHM.GetCollection<HumanConfiguration>("HumanConfiguration");

            #region loging and checking gadget usage
            if (voobj.ContainsKey("dt"))
            {
                object resultEnergi = JsonConvert.DeserializeObject(voobj["dt"].ToString());
                JObject obj = JObject.Parse(resultEnergi.ToString());

                string mac = (obj["mac"] == null) ? "0" : obj["mac"].ToString().ToString();
                string type = (obj["type"] == null) ? "0" : obj["type"].ToString();
                string battery = (obj["battery"] == null) ? "0" : obj["battery"].ToString();
                string heartrate = (obj["heartrate"] == null) ? "0" : obj["heartrate"].ToString();
                string step = (obj["step"] == null) ? "0" : obj["step"].ToString();
                string sleep = (obj["sleep"] == null) ? "0" : obj["sleep"].ToString();
                string calories = (obj["calories"] == null) ? "0" : obj["calories"].ToString();
                string blood = (obj["blood"] == null) ? "0" : obj["blood"].ToString();
                string temperature = (obj["temperature"] == null) ? "0" : obj["temperature"].ToString();
                string sos = (obj["sos"] == null) ? "0" : obj["sos"].ToString();
                string unwear = (obj["unwear"] == null) ? "0" : obj["unwear"].ToString();
                string falling = (obj["falling"] == null) ? "0" : obj["falling"].ToString();
                string nearby = (obj["nearby"] == null) ? "0" : obj["nearby"].ToString();
                string battery_update = (obj["battery_update"] == null) ? "0" : obj["battery_update"].ToString();
                string blood_update = (obj["blood_update"] == null) ? "0" : obj["blood_update"].ToString();
                string temperature_update = (obj["temperature_update"] == null) ? "0" : obj["temperature_update"].ToString();

                BLESmartWatchData voData = new BLESmartWatchData();
                EventsLogMonitoring voDataEvent = new EventsLogMonitoring();

                var GWbuilder = Builders<SmartWatchGatewayView>.Filter;
                var GWflt = GWbuilder.Eq<string>("gatewayID", nearby);
                var GWres = SWgateway.Find(GWflt).SingleOrDefault();
                if (GWres != null)
                {
                    voData.location = GWres.location;
                }
                string[] allowLocation = { "3022", "3016" };
                var PTbuilder = Builders<PersonTagsView>.Filter;
                var PTflt = PTbuilder.And( PTbuilder.Eq<string>("tagID", mac), PTbuilder.Eq<string>("active", "yes"));
                var PTres = PersonTags.Find(PTflt).SingleOrDefault();
                if (PTres != null)
                {
                    var Pbuilder = Builders<PersonView>.Filter;
                    var Pflt = Pbuilder.Eq<string>("personID", PTres.personID);
                    var PWres = Person.Find(Pflt).SingleOrDefault();
                    if (PWres != null)
                    {
                        // loging
                        voData.person = PTres.personID;
                        voData.RecordTimestamp = DateTime.Now;
                        voData.mac = mac;
                        voData.type = type;
                        voData.battery = Convert.ToInt32(battery);
                        voData.heartrate = Convert.ToInt32(heartrate);
                        voData.step = Convert.ToInt32(step);
                        voData.sleep = Convert.ToInt32(sleep);
                        voData.calories = Convert.ToInt32(calories);
                        voData.blood = Convert.ToInt32(blood);
                        voData.temperature = Convert.ToDecimal(temperature);
                        //voData.sos = Convert.ToInt32(sos);
                        //voData.unwear = Convert.ToInt32(unwear);
                        voData.sos = Convert.ToBoolean(sos);
                        voData.unwear = Convert.ToBoolean(unwear);
                        voData.falling = Convert.ToBoolean(falling);
                        voData.nearby = Convert.ToInt32(nearby);
                        voData.battery_update = Convert.ToBoolean(battery_update);
                        voData.blood_update = Convert.ToBoolean(blood_update);
                        voData.temperature_update = Convert.ToBoolean(temperature_update);
                        DataLogs.InsertOne(voData);

                        #region Update Avatar Location Default
                        var avatarID = PWres.avatarID;
                        var defaultZone = PWres.defaultZone;
                        if (voData.location != defaultZone)
                        {
                            //Avatar Location
                            var ALbuilder = Builders<AvatarLocation>.Filter;
                            var ALflt = ALbuilder.Eq<string>("location", voData.location);
                            ALflt &= ALbuilder.Ne<string>("avatarName", PWres.avatarID);
                            var ALres = AvatarLocation.Find(ALflt).Limit(1).FirstOrDefault();

                            avatarID = ALres.avatarName;
                            defaultZone = ALres.location;
                        }
                        #endregion

                        #region get person location
                        PersonLocation dataTG = new PersonLocation();
                        var TGbuilder = Builders<PersonLocation>.Filter;
                        var TGflt = TGbuilder.Eq<string>("personID", PTres.personID);
                        var TGWres = PersonLoc.Find(TGflt).SingleOrDefault();
                        if (TGWres == null)
                        {
                            dataTG.RecordTimestamp = DateTime.Now;
                            dataTG.location = voData.location;
                            dataTG.personID = PTres.personID;
                            dataTG.personName = PWres.personName;
                            dataTG.rssi = voData.nearby.ToString();
                            dataTG.tagID = voData.mac;
                            dataTG.gatewayID = (GWres != null) ? GWres.gatewayName : "";
                            dataTG.type = "SW";
                            dataTG.defaultZone = defaultZone;
                            dataTG.avatarID = avatarID;
                            PersonLoc.InsertOne(dataTG);
                        }
                        else
                        {
                            var update = Builders<PersonLocation>.Update.Set("RecordTimestamp", DateTime.Now)
                            .Set("personID", PTres.personID)
                            .Set("rssi", voData.nearby.ToString())
                            .Set("type", "SW")
                            .Set("location", voData.location)
                            .Set("avatarID", avatarID)
                            .Set("gatewayID", (GWres != null) ? GWres.gatewayName : "");
                            PersonLoc.UpdateOne(TGflt, update);
                        }
                        #endregion

                        #region Check Human Configuration Berjenjang Still DEV
                        //HeartRateLogingDev(voobj, topic, voData, PTres.tagID, PWres.personName + " " + PWres.LastName, PWres.personID);
                        //TemperatureLogingDev(voobj, topic, voData, PTres.tagID, PWres.personName + " " + PWres.LastName, PWres.personID);
                        //SOSLogingDev(voobj, topic, voData, PTres.tagID, PWres.personName + " " + PWres.LastName, PWres.personID);
                        //TemperLogingDev(voobj, topic, voData, PTres.tagID, PWres.personName + " " + PWres.LastName, PWres.personID);
                        #endregion

                        #region Check Human Configuration From Collection Master
                        HeartRateLoging(voobj, topic, voData, PTres.tagID, PWres.personName + " " + PWres.LastName, PWres.personID);
                        TemperatureLoging(voobj, topic, voData, PTres.tagID, PWres.personName + " " + PWres.LastName, PWres.personID);
                        SOSLoging(voobj, topic, voData, PTres.tagID, PWres.personName + " " + PWres.LastName, PWres.personID);
                        TemperLoging(voobj, topic, voData, PTres.tagID, PWres.personName + " " + PWres.LastName, PWres.personID);
                        GeofencingLoging(voobj, topic, voData, PTres.tagID, PWres.personName + " " + PWres.LastName, PWres.personID);
                        #endregion

                    }
                }
            }
            #endregion
        }

        private static void TemperLoging(JObject voobj, string[] topic, BLESmartWatchData voData, string tagID, string personName, string personID)
        {
            EventsLogMonitoring voDataEvent = new EventsLogMonitoring();
            var EventsLog = databaseHM.GetCollection<EventsLogMonitoring>("EventsLog");
            var HumanConfiguration = databaseHM.GetCollection<HumanConfiguration>("HumanConfiguration");
            var EventType = "";
            var filter = new BsonDocument() { };
            var resTemp = HumanConfiguration.Find(filter).FirstOrDefault();
            var EventsLogTemp = databaseHM.GetCollection<EventsLogMonitoringTemp>("EventsLogTemp");
            EventsLogMonitoringTemp dataTemp = new EventsLogMonitoringTemp();
            if (resTemp != null)
            {
                #region 'Check Tamper'
                if (resTemp.activatedTEM == "Y")
                {
                    var timeHRFinish = "-" + resTemp.resetAlertTEM;
                    var datetimeHRFinish = DateTime.Now.AddMinutes(Int64.Parse(timeHRFinish));
                    EventType = "Tampered alert";
                    EventsLogMonitoringTemp resELog = EventsLogTemp.Find(f => f.NRIC == voData.person
                    && f.EventType == EventType).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
                    if (resELog == null)
                    {
                        if (voData.sos)
                        {
                            dataTemp.RecordTimestamp = DateTime.Now;
                            dataTemp.Count = 1;
                            dataTemp.EventType = EventType;
                            dataTemp.TagID = tagID;
                            dataTemp.personName = personName;
                            dataTemp.NRIC = personID;
                            dataTemp.Temp = voData.temperature;
                            dataTemp.HR = voData.heartrate;
                            dataTemp.Location = voData.location;
                            dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                            dataTemp.Acknowledge = "No";
                            dataTemp.ActionRemarks = "Sent response team";
                            dataTemp.Status = "Pending";
                            dataTemp.Geofencing = "No";
                            EventsLogTemp.InsertOne(dataTemp);

                            #region 'masuk collection master'
                            voDataEvent.RecordTimestamp = DateTime.Now;
                            voDataEvent.EventType = EventType;
                            voDataEvent.TagID = tagID;
                            voDataEvent.personName = personName;
                            voDataEvent.NRIC = personID;
                            voDataEvent.Temp = voData.temperature;
                            voDataEvent.HR = voData.heartrate;
                            voDataEvent.Location = voData.location;
                            voDataEvent.Tampered = voData.sos == true ? "Yes" : "No";
                            voDataEvent.Acknowledge = "No";
                            voDataEvent.ActionRemarks = "Sent response team";
                            voDataEvent.Status = "Pending";
                            voDataEvent.Geofencing = "No";
                            EventsLog.InsertOne(voDataEvent);
                            #endregion
                        }
                    }
                    else if (voData.sos == true && resELog != null)
                    {
                        EventsLogMonitoringTemp resELogStart = EventsLogTemp.Find(f => f.NRIC == voData.person
                        && f.EventType == EventType && f.Count == 1).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
                        if (resELogStart.RecordTimestamp < datetimeHRFinish)
                        {
                            var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
                            var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
                            filbuildDelTemp &= buildDelTemp.Eq<string>("EventType", EventType);
                            EventsLogTemp.DeleteMany(filbuildDelTemp);
                        }
                    }
                }
                else
                {
                    var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
                    var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
                    filbuildDelTemp &= buildDelTemp.Regex("EventType", "Tampered alert");
                    EventsLogTemp.DeleteMany(filbuildDelTemp);
                }
                #endregion
            }
        }
        private static void SOSLoging(JObject voobj, string[] topic, BLESmartWatchData voData, string tagID, string personName, string personID)
        {
        }
        public static void HeartRateLoging(JObject voobj, string[] topic, BLESmartWatchData voData, string tagID, string personName, string personID)
        {
            EventsLogMonitoring voDataEvent = new EventsLogMonitoring();
            var EventsLog = databaseHM.GetCollection<EventsLogMonitoring>("EventsLog");
            var HumanConfiguration = databaseHM.GetCollection<HumanConfiguration>("HumanConfiguration");
            var EventType = "";
            var filter = new BsonDocument() { };
            var resTemp = HumanConfiguration.Find(filter).FirstOrDefault();
            var EventsLogTemp = databaseHM.GetCollection<EventsLogMonitoringTemp>("EventsLogTemp");
            
            EventsLogMonitoringTemp dataTemp = new EventsLogMonitoringTemp();
            if (resTemp != null)
            {
                #region 'Check Heart Rate'
                if (resTemp.activatedHR == "Y")
                {
                    var timeHRStart = "-" + resTemp.conPeriodHR;
                    var timeHRReset = "-" + (resTemp.resetAlertHR + resTemp.conPeriodHR);
                    var datetimeHRStart = DateTime.Now.AddMinutes(Int64.Parse(timeHRStart));
                    var datetimeHRReset = DateTime.Now.AddMinutes(Int64.Parse(timeHRReset));
                    EventType = "Low Heart Rate";
                    if (voData.heartrate > resTemp.whenAboveHR)
                    {
                        EventType = "High Heart Rate";
                    }

                    EventsLogMonitoringTemp resELog = EventsLogTemp.Find(f => f.NRIC == voData.person
                    && f.EventType == EventType).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
                    if (resELog == null)
                    { 
                        if (voData.heartrate < resTemp.whenBelowHR || voData.heartrate > resTemp.whenAboveHR)
                        {
                            dataTemp.RecordTimestamp = DateTime.Now;
                            dataTemp.Count = 1;
                            dataTemp.EventType = EventType;
                            dataTemp.TagID = tagID;
                            dataTemp.personName = personName;
                            dataTemp.NRIC = personID;
                            dataTemp.Temp = voData.temperature;
                            dataTemp.HR = voData.heartrate;
                            dataTemp.Location = voData.location;
                            dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                            dataTemp.Acknowledge = "No";
                            dataTemp.ActionRemarks = "Sent response team";
                            dataTemp.Status = "Pending";
                            dataTemp.Geofencing = "No";
                            EventsLogTemp.InsertOne(dataTemp);
                        }
                    }else if (voData.heartrate < resTemp.whenBelowHR || voData.heartrate > resTemp.whenAboveHR && resELog != null)
                    {
                        EventsLogMonitoringTemp resELogStart = EventsLogTemp.Find(f => f.NRIC == voData.person
                        && f.EventType == EventType).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
                        if (resELogStart.RecordTimestamp < datetimeHRStart && resELogStart.Count == 1)
                        {
                            dataTemp.RecordTimestamp = DateTime.Now;
                            dataTemp.Count = resELog.Count + 1;
                            dataTemp.EventType = EventType;
                            dataTemp.TagID = tagID;
                            dataTemp.personName = personName;
                            dataTemp.NRIC = personID;
                            dataTemp.Temp = voData.temperature;
                            dataTemp.HR = voData.heartrate;
                            dataTemp.Location = voData.location;
                            dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                            dataTemp.Acknowledge = "No";
                            dataTemp.ActionRemarks = "Sent response team";
                            dataTemp.Status = "Pending";
                            dataTemp.Geofencing = "No";
                            EventsLogTemp.InsertOne(dataTemp);

                            #region 'masuk collection master'
                            voDataEvent.RecordTimestamp = DateTime.Now;
                            voDataEvent.EventType = EventType;
                            voDataEvent.TagID = tagID;
                            voDataEvent.personName = personName;
                            voDataEvent.NRIC = personID;
                            voDataEvent.Temp = voData.temperature;
                            voDataEvent.HR = voData.heartrate;
                            voDataEvent.Location = voData.location;
                            voDataEvent.Tampered = voData.sos == true ? "Yes" : "No";
                            voDataEvent.Acknowledge = "No";
                            voDataEvent.ActionRemarks = "Sent response team";
                            voDataEvent.Status = "Pending";
                            voDataEvent.Geofencing = "No";
                            EventsLog.InsertOne(voDataEvent);
                            #endregion
                        }
                        else if(resELogStart.RecordTimestamp < datetimeHRStart && resELogStart.Count != 1)
                        {
                            if (resELog.RecordTimestamp < datetimeHRReset)
                            {
                                dataTemp.RecordTimestamp = DateTime.Now;
                                dataTemp.Count = resELog.Count+1;
                                dataTemp.EventType = EventType;
                                dataTemp.TagID = tagID;
                                dataTemp.personName = personName;
                                dataTemp.NRIC = personID;
                                dataTemp.Temp = voData.temperature;
                                dataTemp.HR = voData.heartrate;
                                dataTemp.Location = voData.location;
                                dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                                dataTemp.Acknowledge = "No";
                                dataTemp.ActionRemarks = "Sent response team";
                                dataTemp.Status = "Pending";
                                dataTemp.Geofencing = "No";
                                EventsLogTemp.InsertOne(dataTemp);

                                #region 'masuk collection master'
                                voDataEvent.RecordTimestamp = DateTime.Now;
                                voDataEvent.EventType = EventType;
                                voDataEvent.TagID = tagID;
                                voDataEvent.personName = personName;
                                voDataEvent.NRIC = personID;
                                voDataEvent.Temp = voData.temperature;
                                voDataEvent.HR = voData.heartrate;
                                voDataEvent.Location = voData.location;
                                voDataEvent.Tampered = voData.sos == true ? "Yes" : "No";
                                voDataEvent.Acknowledge = "No";
                                voDataEvent.ActionRemarks = "Sent response team";
                                voDataEvent.Status = "Pending";
                                voDataEvent.Geofencing = "No";
                                EventsLog.InsertOne(voDataEvent);
                                #endregion
                            }
                        }
                    }
                    else if (voData.heartrate > resTemp.whenBelowHR || voData.heartrate < resTemp.whenAboveHR && resELog != null)
                    {
                        var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
                        var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
                        filbuildDelTemp &= buildDelTemp.Eq<string>("EventType", EventType);
                        EventsLogTemp.DeleteMany(filbuildDelTemp);
                    }
                }
                else
                {
                    var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
                    var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
                    filbuildDelTemp &= buildDelTemp.Regex("EventType", "Heart Rate");
                    EventsLogTemp.DeleteMany(filbuildDelTemp);
                }
                #endregion

            }
        }
        public static void TemperatureLoging(JObject voobj, string[] topic, BLESmartWatchData voData, string tagID, string personName, string personID)
        {
            EventsLogMonitoring voDataEvent = new EventsLogMonitoring();
            var EventsLog = databaseHM.GetCollection<EventsLogMonitoring>("EventsLog");
            var HumanConfiguration = databaseHM.GetCollection<HumanConfiguration>("HumanConfiguration");
            var EventType = "";
            var filter = new BsonDocument() { };
            var resTemp = HumanConfiguration.Find(filter).FirstOrDefault();
            var EventsLogTemp = databaseHM.GetCollection<EventsLogMonitoringTemp>("EventsLogTemp");
            EventsLogMonitoringTemp dataTemp = new EventsLogMonitoringTemp();
            if (resTemp != null)
            {
                #region 'Check Heart Rate'
                if (resTemp.activatedTM == "Y")
                {
                    var timeTMStart = "-" + resTemp.conPeriodTM;
                    var timeTMReset = "-" + (resTemp.resetAlertTM + resTemp.conPeriodTM) ;
                    var datetimeTMStart = DateTime.Now.AddMinutes(Int64.Parse(timeTMStart));
                    var datetimeTMReset = DateTime.Now.AddMinutes(Int64.Parse(timeTMReset));
                    EventType = "Low Temperature";
                    if (voData.temperature > resTemp.whenAboveTM)
                    {
                        EventType = "High Temperature";
                    }

                    EventsLogMonitoringTemp resELog = EventsLogTemp.Find(f => f.NRIC == voData.person
                    && f.EventType == EventType).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
                    if (resELog == null)
                    {
                        if (voData.temperature < resTemp.whenBelowTM || voData.temperature > resTemp.whenAboveTM)
                        {
                            dataTemp.RecordTimestamp = DateTime.Now;
                            dataTemp.Count = 1;
                            dataTemp.EventType = EventType;
                            dataTemp.TagID = tagID;
                            dataTemp.personName = personName;
                            dataTemp.NRIC = personID;
                            dataTemp.Temp = voData.temperature;
                            dataTemp.HR = voData.heartrate;
                            dataTemp.Location = voData.location;
                            dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                            dataTemp.Acknowledge = "No";
                            dataTemp.ActionRemarks = "Sent response team";
                            dataTemp.Status = "Pending";
                            dataTemp.Geofencing = "No";
                            EventsLogTemp.InsertOne(dataTemp);
                        }
                    }
                    else if (voData.temperature < resTemp.whenBelowTM || voData.temperature > resTemp.whenAboveTM && resELog != null)
                    {
                        EventsLogMonitoringTemp resELogStart = EventsLogTemp.Find(f => f.NRIC == voData.person
                        && f.EventType == EventType).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
                        if (resELogStart.RecordTimestamp < datetimeTMStart && resELogStart.Count == 1)
                        {
                            dataTemp.RecordTimestamp = DateTime.Now;
                            dataTemp.Count = resELog.Count + 1;
                            dataTemp.EventType = EventType;
                            dataTemp.TagID = tagID;
                            dataTemp.personName = personName;
                            dataTemp.NRIC = personID;
                            dataTemp.Temp = voData.temperature;
                            dataTemp.HR = voData.heartrate;
                            dataTemp.Location = voData.location;
                            dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                            dataTemp.Acknowledge = "No";
                            dataTemp.ActionRemarks = "Sent response team";
                            dataTemp.Status = "Pending";
                            dataTemp.Geofencing = "No";
                            EventsLogTemp.InsertOne(dataTemp);

                            #region 'masuk collection master'
                            voDataEvent.RecordTimestamp = DateTime.Now;
                            voDataEvent.EventType = EventType;
                            voDataEvent.TagID = tagID;
                            voDataEvent.personName = personName;
                            voDataEvent.NRIC = personID;
                            voDataEvent.Temp = voData.temperature;
                            voDataEvent.HR = voData.heartrate;
                            voDataEvent.Location = voData.location;
                            voDataEvent.Tampered = voData.sos == true ? "Yes" : "No";
                            voDataEvent.Acknowledge = "No";
                            voDataEvent.ActionRemarks = "Sent response team";
                            voDataEvent.Status = "Pending";
                            voDataEvent.Geofencing = "No";
                            EventsLog.InsertOne(voDataEvent);
                            #endregion
                        }
                        else if (resELogStart.RecordTimestamp < datetimeTMStart && resELogStart.Count != 1) 
                        {
                            if (resELog.RecordTimestamp < datetimeTMReset)
                            {
                                dataTemp.RecordTimestamp = DateTime.Now;
                                dataTemp.Count = resELog.Count + 1;
                                dataTemp.EventType = EventType;
                                dataTemp.TagID = tagID;
                                dataTemp.personName = personName;
                                dataTemp.NRIC = personID;
                                dataTemp.Temp = voData.temperature;
                                dataTemp.HR = voData.heartrate;
                                dataTemp.Location = voData.location;
                                dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                                dataTemp.Acknowledge = "No";
                                dataTemp.ActionRemarks = "Sent response team";
                                dataTemp.Status = "Pending";
                                dataTemp.Geofencing = "No";
                                EventsLogTemp.InsertOne(dataTemp);

                                #region 'masuk collection master'
                                voDataEvent.RecordTimestamp = DateTime.Now;
                                voDataEvent.EventType = EventType;
                                voDataEvent.TagID = tagID;
                                voDataEvent.personName = personName;
                                voDataEvent.NRIC = personID;
                                voDataEvent.Temp = voData.temperature;
                                voDataEvent.HR = voData.heartrate;
                                voDataEvent.Location = voData.location;
                                voDataEvent.Tampered = voData.sos == true ? "Yes" : "No";
                                voDataEvent.Acknowledge = "No";
                                voDataEvent.ActionRemarks = "Sent response team";
                                voDataEvent.Status = "Pending";
                                voDataEvent.Geofencing = "No";
                                EventsLog.InsertOne(voDataEvent);
                                #endregion
                            }
                        }
                    }
                    else if (voData.temperature > resTemp.whenBelowTM || voData.temperature < resTemp.whenAboveTM && resELog != null)
                    {
                        var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
                        var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
                        filbuildDelTemp &= buildDelTemp.Eq<string>("EventType", EventType);
                        EventsLogTemp.DeleteMany(filbuildDelTemp);
                    }
                }
                else
                {
                    var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
                    var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
                    filbuildDelTemp &= buildDelTemp.Regex("EventType", "Temperature");
                    EventsLogTemp.DeleteMany(filbuildDelTemp);
                }
                #endregion

            }
        }

        public static void GeofencingLoging(JObject voobj, string[] topic, BLESmartWatchData voData, string tagID, string personName, string personID)
        {
            EventsLogMonitoring voDataEvent = new EventsLogMonitoring();
            var EventsLog = databaseHM.GetCollection<EventsLogMonitoring>("EventsLog");
            var HumanConfiguration = databaseHM.GetCollection<HumanConfiguration>("HumanConfiguration");
            var DataLogs = databaseHM.GetCollection<SmartWatchDataLog>("SmartwatchDataLogs");
            var Person = databaseHM.GetCollection<PersonView>("Person");
            var EventType = "";
            var EventTypeZone = "";
            var filter = new BsonDocument() { };
            var resTemp = HumanConfiguration.Find(filter).FirstOrDefault();
            var resPerson = Person.Find(f => f.personID == personID).FirstOrDefault();
            var resDataLogs = DataLogs.Find(f => f.person == personID).Limit(1).SortByDescending(x => x.RecordTimestamp).FirstOrDefault();
            var EventsLogTemp = databaseHM.GetCollection<EventsLogMonitoringTemp>("EventsLogTemp");
            EventsLogMonitoringTemp dataTemp = new EventsLogMonitoringTemp();
            if (resTemp != null)
            {
                #region 'Check Status Location'
                if (resTemp.activatedGeofencing == "Y")
                {
                    var timeGeofencingStart = "-" + resTemp.conPeriodGeofencing;
                    var timeGeofencingFinish = "-" + resTemp.resetAlertGeofencing;
                    var datetimeGeofencingStart = DateTime.Now.AddSeconds(Int64.Parse(timeGeofencingStart));
                    var datetimeGeofencingFinish = DateTime.Now.AddMinutes(Int64.Parse(timeGeofencingFinish)).AddSeconds(Int64.Parse(timeGeofencingStart));

                    #region Check not detected
                    EventType = "Geofencing not detected";
                    EventsLogMonitoringTemp resELog = EventsLogTemp.Find(f => f.NRIC == voData.person
                    && f.EventType == EventType).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
                    if (resELog == null)
                    {
                        if (resDataLogs.RecordTimestamp < datetimeGeofencingStart)
                        {
                            dataTemp.RecordTimestamp = DateTime.Now;
                            dataTemp.Count = 1;
                            dataTemp.EventType = EventType;
                            dataTemp.TagID = tagID;
                            dataTemp.personName = personName;
                            dataTemp.NRIC = personID;
                            dataTemp.Temp = voData.temperature;
                            dataTemp.HR = voData.heartrate;
                            dataTemp.Location = voData.location;
                            dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                            dataTemp.Acknowledge = "No";
                            dataTemp.ActionRemarks = "Sent response team";
                            dataTemp.Status = "Pending";
                            dataTemp.Geofencing = "Yes";
                            EventsLogTemp.InsertOne(dataTemp);
                        }
                    }
                    else if (resDataLogs.RecordTimestamp < datetimeGeofencingStart && resELog != null)
                    {
                        EventsLogMonitoringTemp resELogStart = EventsLogTemp.Find(f => f.NRIC == voData.person
                        && f.EventType == EventType).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
                        if (resELogStart.RecordTimestamp < datetimeGeofencingStart && resELogStart.Count == 1)
                        {
                            dataTemp.RecordTimestamp = DateTime.Now;
                            dataTemp.Count = resELog.Count + 1;
                            dataTemp.EventType = EventType;
                            dataTemp.TagID = tagID;
                            dataTemp.personName = personName;
                            dataTemp.NRIC = personID;
                            dataTemp.Temp = voData.temperature;
                            dataTemp.HR = voData.heartrate;
                            dataTemp.Location = voData.location;
                            dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                            dataTemp.Acknowledge = "No";
                            dataTemp.ActionRemarks = "Sent response team";
                            dataTemp.Status = "Pending";
                            dataTemp.Geofencing = "Yes";
                            EventsLogTemp.InsertOne(dataTemp);

                            #region 'masuk collection master'
                            voDataEvent.RecordTimestamp = DateTime.Now;
                            voDataEvent.EventType = EventType;
                            voDataEvent.TagID = tagID;
                            voDataEvent.personName = personName;
                            voDataEvent.NRIC = personID;
                            voDataEvent.Temp = voData.temperature;
                            voDataEvent.HR = voData.heartrate;
                            voDataEvent.Location = voData.location;
                            voDataEvent.Tampered = voData.sos == true ? "Yes" : "No";
                            voDataEvent.Acknowledge = "No";
                            voDataEvent.ActionRemarks = "Sent response team";
                            voDataEvent.Status = "Pending";
                            voDataEvent.Geofencing = "Yes";
                            EventsLog.InsertOne(voDataEvent);
                            #endregion
                        }
                        else if (resELogStart.RecordTimestamp < datetimeGeofencingStart && resELogStart.Count != 1)
                        {
                            if (resELog.RecordTimestamp < datetimeGeofencingFinish)
                            {
                                dataTemp.RecordTimestamp = DateTime.Now;
                                dataTemp.Count = resELog.Count + 1;
                                dataTemp.EventType = EventType;
                                dataTemp.TagID = tagID;
                                dataTemp.personName = personName;
                                dataTemp.NRIC = personID;
                                dataTemp.Temp = voData.temperature;
                                dataTemp.HR = voData.heartrate;
                                dataTemp.Location = voData.location;
                                dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                                dataTemp.Acknowledge = "No";
                                dataTemp.ActionRemarks = "Sent response team";
                                dataTemp.Status = "Pending";
                                dataTemp.Geofencing = "Yes";
                                EventsLogTemp.InsertOne(dataTemp);

                                #region 'masuk collection master'
                                voDataEvent.RecordTimestamp = DateTime.Now;
                                voDataEvent.EventType = EventType;
                                voDataEvent.TagID = tagID;
                                voDataEvent.personName = personName;
                                voDataEvent.NRIC = personID;
                                voDataEvent.Temp = voData.temperature;
                                voDataEvent.HR = voData.heartrate;
                                voDataEvent.Location = voData.location;
                                voDataEvent.Tampered = voData.sos == true ? "Yes" : "No";
                                voDataEvent.Acknowledge = "No";
                                voDataEvent.ActionRemarks = "Sent response team";
                                voDataEvent.Status = "Pending";
                                voDataEvent.Geofencing = "Yes";
                                EventsLog.InsertOne(voDataEvent);
                                #endregion
                            }
                        }
                        else if (resDataLogs.RecordTimestamp > datetimeGeofencingStart && resELog != null)
                        {
                            var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
                            var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
                            filbuildDelTemp &= buildDelTemp.Eq<string>("EventType", EventType);
                            EventsLogTemp.DeleteMany(filbuildDelTemp);
                        }
                    }
                    #endregion

                    #region Check Zone
                    EventTypeZone = "Geofencing alert zone";
                    EventsLogMonitoringTemp resELogZone = EventsLogTemp.Find(f => f.NRIC == voData.person
                    && f.EventType == EventTypeZone).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
                    if (resELogZone == null)
                    {
                        if (voData.location != resPerson.defaultZone)
                        {
                            dataTemp.RecordTimestamp = DateTime.Now;
                            dataTemp.Count = 1;
                            dataTemp.EventType = EventTypeZone;
                            dataTemp.TagID = tagID;
                            dataTemp.personName = personName;
                            dataTemp.NRIC = personID;
                            dataTemp.Temp = voData.temperature;
                            dataTemp.HR = voData.heartrate;
                            dataTemp.Location = voData.location;
                            dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                            dataTemp.Acknowledge = "No";
                            dataTemp.ActionRemarks = "Sent response team";
                            dataTemp.Status = "Pending";
                            dataTemp.Geofencing = "Yes";
                            EventsLogTemp.InsertOne(dataTemp);
                        }
                    }
                    else if (voData.location != resPerson.defaultZone && resELogZone != null)
                    {
                        EventsLogMonitoringTemp resELogStartZone = EventsLogTemp.Find(f => f.NRIC == voData.person
                        && f.EventType == EventTypeZone).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
                        if (resELogStartZone.RecordTimestamp < datetimeGeofencingStart && resELogStartZone.Count == 1)
                        {
                            dataTemp.RecordTimestamp = DateTime.Now;
                            dataTemp.Count = resELogZone.Count + 1;
                            dataTemp.EventType = EventTypeZone;
                            dataTemp.TagID = tagID;
                            dataTemp.personName = personName;
                            dataTemp.NRIC = personID;
                            dataTemp.Temp = voData.temperature;
                            dataTemp.HR = voData.heartrate;
                            dataTemp.Location = voData.location;
                            dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                            dataTemp.Acknowledge = "No";
                            dataTemp.ActionRemarks = "Sent response team";
                            dataTemp.Status = "Pending";
                            dataTemp.Geofencing = "Yes";
                            EventsLogTemp.InsertOne(dataTemp);

                            #region 'masuk collection master'
                            voDataEvent.RecordTimestamp = DateTime.Now;
                            voDataEvent.EventType = EventTypeZone;
                            voDataEvent.TagID = tagID;
                            voDataEvent.personName = personName;
                            voDataEvent.NRIC = personID;
                            voDataEvent.Temp = voData.temperature;
                            voDataEvent.HR = voData.heartrate;
                            voDataEvent.Location = voData.location;
                            voDataEvent.Tampered = voData.sos == true ? "Yes" : "No";
                            voDataEvent.Acknowledge = "No";
                            voDataEvent.ActionRemarks = "Sent response team";
                            voDataEvent.Status = "Pending";
                            voDataEvent.Geofencing = "Yes";
                            EventsLog.InsertOne(voDataEvent);
                            #endregion
                        }
                        else if (resELogStartZone.RecordTimestamp < datetimeGeofencingStart && resELogStartZone.Count != 1)
                        {
                            if (resELogZone.RecordTimestamp < datetimeGeofencingFinish)
                            {
                                dataTemp.RecordTimestamp = DateTime.Now;
                                dataTemp.Count = resELogZone.Count + 1;
                                dataTemp.EventType = EventTypeZone;
                                dataTemp.TagID = tagID;
                                dataTemp.personName = personName;
                                dataTemp.NRIC = personID;
                                dataTemp.Temp = voData.temperature;
                                dataTemp.HR = voData.heartrate;
                                dataTemp.Location = voData.location;
                                dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
                                dataTemp.Acknowledge = "No";
                                dataTemp.ActionRemarks = "Sent response team";
                                dataTemp.Status = "Pending";
                                dataTemp.Geofencing = "Yes";
                                EventsLogTemp.InsertOne(dataTemp);

                                #region 'masuk collection master'
                                voDataEvent.RecordTimestamp = DateTime.Now;
                                voDataEvent.EventType = EventTypeZone;
                                voDataEvent.TagID = tagID;
                                voDataEvent.personName = personName;
                                voDataEvent.NRIC = personID;
                                voDataEvent.Temp = voData.temperature;
                                voDataEvent.HR = voData.heartrate;
                                voDataEvent.Location = voData.location;
                                voDataEvent.Tampered = voData.sos == true ? "Yes" : "No";
                                voDataEvent.Acknowledge = "No";
                                voDataEvent.ActionRemarks = "Sent response team";
                                voDataEvent.Status = "Pending";
                                voDataEvent.Geofencing = "Yes";
                                EventsLog.InsertOne(voDataEvent);
                                #endregion
                            }
                        }
                        else if (voData.location == resPerson.defaultZone && resELogZone != null)
                        {
                            var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
                            var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
                            filbuildDelTemp &= buildDelTemp.Eq<string>("EventType", EventTypeZone);
                            EventsLogTemp.DeleteMany(filbuildDelTemp);
                        }
                        #endregion
                    }
                }
                else
                {
                    var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
                    var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
                    filbuildDelTemp &= buildDelTemp.Regex("EventType", "Geofencing alert");
                    EventsLogTemp.DeleteMany(filbuildDelTemp);
                }
                #endregion

            }
        }

        #region 'Still Dev'
        //private static void TemperLogingDev(JObject voobj, string[] topic, BLESmartWatchData voData, string tagID, string personName, string personID)
        //{
        //    EventsLogMonitoring voDataEvent = new EventsLogMonitoring();
        //    var EventsLog = databaseHM.GetCollection<EventsLogMonitoring>("EventsLog");
        //    var HumanConfiguration = databaseHM.GetCollection<HumanConfiguration>("HumanConfiguration");
        //    var EventType = "";
        //    var filter = new BsonDocument() { };
        //    var HCres = HumanConfiguration.Find(filter).FirstOrDefault();
        //    var EventsLogTemp = databaseHM.GetCollection<EventsLogMonitoringTemp>("EventsLogTemp");
        //    #region 'Check Configuration'
        //    var HumanConfigurationTemp = databaseHM.GetCollection<HumanConfigurationTemp>("HumanConfigurationTemp");
        //    HumanConfigurationTemp resTempRun = HumanConfigurationTemp.Find(f => f.tipe == "TEM").Limit(1).FirstOrDefault();
        //    if (resTempRun != null)
        //    {
        //        List<HumanConfigurationTemp> CresTempRuning = HumanConfigurationTemp.Find(f => f.tipe == "TEM" && f.currentStatus == "Running").ToList();
        //        List<HumanConfigurationTemp> CresTempRun = HumanConfigurationTemp.Find(f => f.tipe == "TEM" && f.currentStatus == "Waiting").ToList();
        //        if (CresTempRun.Count() > 0 && CresTempRuning.Count() == 0)
        //        {
        //            var builderCon = Builders<HumanConfigurationTemp>.Filter;
        //            var filCon = builderCon.Eq<ObjectId>("_id", CresTempRun[0]._id);
        //            var updateConf = Builders<HumanConfigurationTemp>.Update.Set("currentStatus", "Running");
        //            HumanConfigurationTemp.UpdateOne(filCon, updateConf);
        //        }
        //    }
        //    else
        //    {
        //        HumanConfigurationTemp oData = new HumanConfigurationTemp();
        //        oData.recordTimestamp = DateTime.Now;
        //        oData.status = HCres.activatedTEM;
        //        oData.tipe = "TEM";
        //        oData.below = 0;
        //        oData.above = 0;
        //        oData.periode =0;
        //        oData.reset = HCres.resetAlertTEM;
        //        oData.currentStatus = "Running";
        //        HumanConfigurationTemp.InsertOne(oData);
        //    }
        //    #endregion

        //    var HumanConfigurationTempRun = databaseHM.GetCollection<HumanConfigurationTemp>("HumanConfigurationTemp");
        //    HumanConfigurationTemp resTemp = HumanConfigurationTemp.Find(f => f.tipe == "TEM" && f.currentStatus == "Running").Limit(1).FirstOrDefault();
        //    EventsLogMonitoringTemp dataTemp = new EventsLogMonitoringTemp();
        //    if (resTemp != null)
        //    {
        //        #region 'Check Tamper'
        //        if (resTemp.status == "Y")
        //        {
        //            bool allowInsert = false;
        //            bool allowReset = false;
        //            EventType = "Tampered alert";
        //            String currentTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:00");
        //            int resetTime = 1;
        //            if (resTemp.periode == 0)
        //            {
        //                currentTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //                resetTime = 60;
        //            }
        //            EventsLogMonitoringTemp resELog = EventsLogTemp.Find(f => f.NRIC == voData.person
        //            && f.RecordTimestamp == currentTime && f.EventType == EventType).Limit(1).FirstOrDefault();
        //            if (resELog == null)
        //            {
        //                EventsLogMonitoringTemp resELogCheck = EventsLogTemp.Find(f => f.NRIC == voData.person
        //                && f.EventType == EventType).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
        //                int count = 1;
        //                if (resELogCheck != null)
        //                {
        //                    count = resELogCheck.Count + 1;
        //                    if (count > (resTemp.reset * resetTime))
        //                    {
        //                        count = 1;
        //                        allowReset = true;
        //                    }
        //                }

        //                dataTemp.RecordTimestamp = currentTime;
        //                dataTemp.Count = count;
        //                dataTemp.EventType = EventType;
        //                dataTemp.TagID = tagID;
        //                dataTemp.personName = personName;
        //                dataTemp.NRIC = personID;
        //                dataTemp.Temp = voData.temperature;
        //                dataTemp.HR = voData.heartrate;
        //                dataTemp.Location = voData.location;
        //                dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
        //                dataTemp.Acknowledge = "No";
        //                dataTemp.ActionRemarks = "Sent response team";
        //                dataTemp.Status = "Pending";
        //                EventsLogTemp.InsertOne(dataTemp);

        //                if (resTemp.periode == 0)
        //                {
        //                    allowInsert = true;
        //                }
        //                else
        //                {
        //                    if (count % resTemp.periode == 0)
        //                    {
        //                        allowInsert = true;
        //                    }
        //                }

        //                if (allowInsert)
        //                {
        //                    if (voData.sos)
        //                    {
        //                        #region 'masuk collection master'
        //                        voDataEvent.RecordTimestamp = DateTime.Now;
        //                        voDataEvent.EventType = EventType;
        //                        voDataEvent.TagID = tagID;
        //                        voDataEvent.personName = personName;
        //                        voDataEvent.NRIC = personID;
        //                        voDataEvent.Temp = voData.temperature;
        //                        voDataEvent.HR = voData.heartrate;
        //                        voDataEvent.Location = voData.location;
        //                        voDataEvent.Tampered = voData.sos == true ? "Yes" : "No";
        //                        voDataEvent.Acknowledge = "No";
        //                        voDataEvent.ActionRemarks = "Sent response team";
        //                        voDataEvent.Status = "Pending";
        //                        EventsLog.InsertOne(voDataEvent);
        //                        #endregion
        //                    }
        //                }

        //                if (allowReset)
        //                {
        //                    var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
        //                    var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
        //                    filbuildDelTemp &= buildDelTemp.Eq<string>("EventType", EventType);
        //                    EventsLogTemp.DeleteMany(filbuildDelTemp);

        //                    var builderCon = Builders<HumanConfigurationTemp>.Filter;
        //                    var filCon = builderCon.Eq<ObjectId>("_id", resTemp._id);
        //                    HumanConfigurationTemp.DeleteOne(filCon);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
        //            var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
        //            filbuildDelTemp &= buildDelTemp.Regex("EventType", "Tampered alert");
        //            EventsLogTemp.DeleteMany(filbuildDelTemp);
        //        }
        //        #endregion

        //    }
        //}
        //private static void SOSLogingDev(JObject voobj, string[] topic, BLESmartWatchData voData, string tagID, string personName, string personID)
        //{
        //}
        //public static void HeartRateLogingDev(JObject voobj, string[] topic, BLESmartWatchData voData, string tagID, string personName, string personID)
        //{
        //    EventsLogMonitoring voDataEvent = new EventsLogMonitoring();
        //    var EventsLog = databaseHM.GetCollection<EventsLogMonitoring>("EventsLog");
        //    var HumanConfiguration = databaseHM.GetCollection<HumanConfiguration>("HumanConfiguration");
        //    var EventType = "";
        //    var filter = new BsonDocument() { };
        //    var HCres = HumanConfiguration.Find(filter).FirstOrDefault();
        //    var EventsLogTemp = databaseHM.GetCollection<EventsLogMonitoringTemp>("EventsLogTemp");
        //    #region 'Check Configuration'
        //    var HumanConfigurationTemp = databaseHM.GetCollection<HumanConfigurationTemp>("HumanConfigurationTemp");
        //    HumanConfigurationTemp resTempRun = HumanConfigurationTemp.Find(f => f.tipe == "HR").Limit(1).FirstOrDefault();
        //    if (resTempRun != null)
        //    {

        //        List<HumanConfigurationTemp> CresTempRuning = HumanConfigurationTemp.Find(f => f.tipe == "HR" && f.currentStatus == "Running").ToList();
        //        List<HumanConfigurationTemp> CresTempRun = HumanConfigurationTemp.Find(f => f.tipe == "HR" && f.currentStatus == "Waiting").ToList();
        //        if (CresTempRun.Count() > 0 && CresTempRuning.Count() == 0)
        //        {
        //            var builderCon = Builders<HumanConfigurationTemp>.Filter;
        //            var filCon = builderCon.Eq<ObjectId>("_id", CresTempRun[0]._id);
        //            var updateConf = Builders<HumanConfigurationTemp>.Update.Set("currentStatus", "Running");
        //            HumanConfigurationTemp.UpdateOne(filCon, updateConf);
        //        }
        //    }
        //    else
        //    {
        //        HumanConfigurationTemp oData = new HumanConfigurationTemp();
        //        oData.recordTimestamp = DateTime.Now;
        //        oData.status = HCres.activatedHR;
        //        oData.tipe = "HR";
        //        oData.below = HCres.whenBelowHR;
        //        oData.above = HCres.whenAboveHR;
        //        oData.periode = HCres.conPeriodHR;
        //        oData.reset = HCres.resetAlertHR;
        //        oData.currentStatus = "Running";
        //        HumanConfigurationTemp.InsertOne(oData);
        //    }
        //    #endregion

        //    var HumanConfigurationTempRun = databaseHM.GetCollection<HumanConfigurationTemp>("HumanConfigurationTemp");
        //    HumanConfigurationTemp resTemp = HumanConfigurationTemp.Find(f => f.tipe == "HR" && f.currentStatus == "Running").Limit(1).FirstOrDefault();
        //    EventsLogMonitoringTemp dataTemp = new EventsLogMonitoringTemp();
        //    if (resTemp != null)
        //    {
        //        #region 'Check Heart Rate'
        //        if (resTemp.status == "Y")
        //        {
        //            bool allowInsert = false;
        //            bool allowReset = false;
        //            EventType = "Low Heart Rate";
        //            if (voData.heartrate > resTemp.above)
        //            {
        //                EventType = "High Heart Rate";
        //            }
        //            String currentTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:00");
        //            int resetTime = 1;
        //            if(resTemp.periode == 0)
        //            {
        //                currentTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //                resetTime = 60;
        //            }
        //            EventsLogMonitoringTemp resELog = EventsLogTemp.Find(f => f.NRIC == voData.person
        //            && f.RecordTimestamp == currentTime && f.EventType == EventType).Limit(1).FirstOrDefault();
        //            if (resELog == null)
        //            {
        //                EventsLogMonitoringTemp resELogCheck = EventsLogTemp.Find(f => f.NRIC == voData.person
        //                && f.EventType == EventType).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
        //                int count = 1;
        //                if (resELogCheck != null)
        //                {
        //                    count = resELogCheck.Count + 1;
        //                    if (count > (resTemp.reset * resetTime))
        //                    {
        //                        count = 1;
        //                        allowReset = true;
        //                    }
        //                }

        //                dataTemp.RecordTimestamp = currentTime;
        //                dataTemp.Count = count;
        //                dataTemp.EventType = EventType;
        //                dataTemp.TagID = tagID;
        //                dataTemp.personName = personName;
        //                dataTemp.NRIC = personID;
        //                dataTemp.Temp = voData.temperature;
        //                dataTemp.HR = voData.heartrate;
        //                dataTemp.Location = voData.location;
        //                dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
        //                dataTemp.Acknowledge = "No";
        //                dataTemp.ActionRemarks = "Sent response team";
        //                dataTemp.Status = "Pending";
        //                EventsLogTemp.InsertOne(dataTemp);

        //                if (resTemp.periode == 0)
        //                {
        //                    allowInsert = true;
        //                }
        //                else
        //                {
        //                    if (count % resTemp.periode == 0)
        //                    {
        //                        allowInsert = true;
        //                    }
        //                }

        //                if (allowInsert)
        //                {
        //                    if (voData.heartrate < resTemp.below || voData.heartrate > resTemp.above)
        //                    {
        //                        #region 'masuk collection master'
        //                        voDataEvent.RecordTimestamp = DateTime.Now;
        //                        voDataEvent.EventType = EventType;
        //                        voDataEvent.TagID = tagID;
        //                        voDataEvent.personName = personName;
        //                        voDataEvent.NRIC = personID;
        //                        voDataEvent.Temp = voData.temperature;
        //                        voDataEvent.HR = voData.heartrate;
        //                        voDataEvent.Location = voData.location;
        //                        voDataEvent.Tampered = voData.sos == true ? "Yes" : "No";
        //                        voDataEvent.Acknowledge = "No";
        //                        voDataEvent.ActionRemarks = "Sent response team";
        //                        voDataEvent.Status = "Pending";
        //                        EventsLog.InsertOne(voDataEvent);
        //                        #endregion
        //                    }
        //                }

        //                if (allowReset)
        //                {
        //                    var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
        //                    var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
        //                    filbuildDelTemp &= buildDelTemp.Eq<string>("EventType", EventType);
        //                    EventsLogTemp.DeleteMany(filbuildDelTemp);

        //                    var builderCon = Builders<HumanConfigurationTemp>.Filter;
        //                    var filCon = builderCon.Eq<ObjectId>("_id", resTemp._id);
        //                    HumanConfigurationTemp.DeleteOne(filCon);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
        //            var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
        //            filbuildDelTemp &= buildDelTemp.Regex("EventType", "Heart Rate");
        //            EventsLogTemp.DeleteMany(filbuildDelTemp);
        //        }
        //        #endregion

        //    }
        //}
        //public static void TemperatureLogingDev(JObject voobj, string[] topic, BLESmartWatchData voData, string tagID, string personName, string personID)
        //{
        //    EventsLogMonitoring voDataEvent = new EventsLogMonitoring();
        //    var EventsLog = databaseHM.GetCollection<EventsLogMonitoring>("EventsLog");
        //    var HumanConfiguration = databaseHM.GetCollection<HumanConfiguration>("HumanConfiguration");
        //    var EventType = "";
        //    var filter = new BsonDocument() { };
        //    var HCres = HumanConfiguration.Find(filter).FirstOrDefault();
        //    var EventsLogTemp = databaseHM.GetCollection<EventsLogMonitoringTemp>("EventsLogTemp");
        //    #region 'Check Configuration'
        //    var HumanConfigurationTemp = databaseHM.GetCollection<HumanConfigurationTemp>("HumanConfigurationTemp");
        //    HumanConfigurationTemp resTempRun = HumanConfigurationTemp.Find(f => f.tipe == "TM").Limit(1).FirstOrDefault();
        //    if (resTempRun != null)
        //    {
        //        List<HumanConfigurationTemp> CresTempRuning = HumanConfigurationTemp.Find(f => f.tipe == "TM" && f.currentStatus == "Running").ToList();
        //        List<HumanConfigurationTemp> CresTempRun = HumanConfigurationTemp.Find(f => f.tipe == "TM" && f.currentStatus == "Waiting").ToList();
        //        if (CresTempRun.Count() > 0 && CresTempRuning.Count() == 0)
        //        {
        //            var builderCon = Builders<HumanConfigurationTemp>.Filter;
        //            var filCon = builderCon.Eq<ObjectId>("_id", CresTempRun[0]._id);
        //            var updateConf = Builders<HumanConfigurationTemp>.Update.Set("currentStatus", "Running");
        //            HumanConfigurationTemp.UpdateOne(filCon, updateConf);
        //        }
        //    }
        //    else
        //    {
        //        HumanConfigurationTemp oData = new HumanConfigurationTemp();
        //        oData.recordTimestamp = DateTime.Now;
        //        oData.status = HCres.activatedTM;
        //        oData.tipe = "TM";
        //        oData.below = HCres.whenBelowTM;
        //        oData.above = HCres.whenAboveTM;
        //        oData.periode = HCres.conPeriodTM;
        //        oData.reset = HCres.resetAlertTM;
        //        oData.currentStatus = "Running";
        //        HumanConfigurationTemp.InsertOne(oData);
        //    }
        //    #endregion

        //    var HumanConfigurationTempRun = databaseHM.GetCollection<HumanConfigurationTemp>("HumanConfigurationTemp");
        //    HumanConfigurationTemp resTemp = HumanConfigurationTemp.Find(f => f.tipe == "TM" && f.currentStatus == "Running").Limit(1).FirstOrDefault();
        //    EventsLogMonitoringTemp dataTemp = new EventsLogMonitoringTemp();
        //    if (resTemp != null)
        //    {
        //        #region 'Check Heart Rate'
        //        if (resTemp.status == "Y")
        //        {
        //            bool allowInsert = false;
        //            bool allowReset = false;
        //            EventType = "Low Temperature";
        //            if (voData.temperature > resTemp.above)
        //            {
        //                EventType = "High Temperature";
        //            }
        //            String currentTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:00");
        //            int resetTime = 1;
        //            if (resTemp.periode == 0)
        //            {
        //                currentTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //                resetTime = 60;
        //            }
        //            EventsLogMonitoringTemp resELog = EventsLogTemp.Find(f => f.NRIC == voData.person
        //            && f.RecordTimestamp == currentTime && f.EventType == EventType).Limit(1).FirstOrDefault();
        //            if (resELog == null)
        //            {
        //                EventsLogMonitoringTemp resELogCheck = EventsLogTemp.Find(f => f.NRIC == voData.person
        //                && f.EventType == EventType).Limit(1).SortByDescending(x => x._id).FirstOrDefault();
        //                int count = 1;
        //                if (resELogCheck != null)
        //                {
        //                    count = resELogCheck.Count + 1;
        //                    if (count > (resTemp.reset * resetTime))
        //                    {
        //                        count = 1;
        //                        allowReset = true;
        //                    }
        //                }

        //                dataTemp.RecordTimestamp = currentTime;
        //                dataTemp.Count = count;
        //                dataTemp.EventType = EventType;
        //                dataTemp.TagID = tagID;
        //                dataTemp.personName = personName;
        //                dataTemp.NRIC = personID;
        //                dataTemp.Temp = voData.temperature;
        //                dataTemp.HR = voData.heartrate;
        //                dataTemp.Location = voData.location;
        //                dataTemp.Tampered = voData.sos == true ? "Yes" : "No";
        //                dataTemp.Acknowledge = "No";
        //                dataTemp.ActionRemarks = "Sent response team";
        //                dataTemp.Status = "Pending";
        //                EventsLogTemp.InsertOne(dataTemp);

        //                if (resTemp.periode == 0)
        //                {
        //                    allowInsert = true;
        //                }
        //                else
        //                {
        //                    if (count % resTemp.periode == 0)
        //                    {
        //                        allowInsert = true;
        //                    }
        //                }

        //                if (allowInsert)
        //                {
        //                    if (voData.temperature < resTemp.below || voData.temperature > resTemp.above)
        //                    {
        //                        #region 'masuk collection master'
        //                        voDataEvent.RecordTimestamp = DateTime.Now;
        //                        voDataEvent.EventType = EventType;
        //                        voDataEvent.TagID = tagID;
        //                        voDataEvent.personName = personName;
        //                        voDataEvent.NRIC = personID;
        //                        voDataEvent.Temp = voData.temperature;
        //                        voDataEvent.HR = voData.heartrate;
        //                        voDataEvent.Location = voData.location;
        //                        voDataEvent.Tampered = voData.sos == true ? "Yes" : "No";
        //                        voDataEvent.Acknowledge = "No";
        //                        voDataEvent.ActionRemarks = "Sent response team";
        //                        voDataEvent.Status = "Pending";
        //                        EventsLog.InsertOne(voDataEvent);
        //                        #endregion
        //                    }
        //                }

        //                if (allowReset)
        //                {
        //                    var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
        //                    var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
        //                    filbuildDelTemp &= buildDelTemp.Eq<string>("EventType", EventType);
        //                    EventsLogTemp.DeleteMany(filbuildDelTemp);

        //                    var builderCon = Builders<HumanConfigurationTemp>.Filter;
        //                    var filCon = builderCon.Eq<ObjectId>("_id", resTemp._id);
        //                    HumanConfigurationTemp.DeleteOne(filCon);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            var buildDelTemp = Builders<EventsLogMonitoringTemp>.Filter;
        //            var filbuildDelTemp = buildDelTemp.Eq<string>("NRIC", voData.person);
        //            filbuildDelTemp &= buildDelTemp.Regex("EventType", "Temperature");
        //            EventsLogTemp.DeleteMany(filbuildDelTemp);
        //        }
        //        #endregion

        //    }
        //}
        #endregion


        private static void setConfig(string message)
        {
            try
            {
                object result = JsonConvert.DeserializeObject(message);
                JObject voobj = JObject.Parse(result.ToString());
                string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";

                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("mongodb_server", voobj["mongodb_server"].ToString());
                voResData.Add("signalr_server", voobj["signalr_server"].ToString());
                voResData.Add("MQTTBroker_1", voobj["MQTTBroker_1"].ToString());
                voResData.Add("MQTTBroker_2", voobj["MQTTBroker_2"].ToString());
                voResData.Add("MQTTTopic_1", voobj["MQTTTopic_1"].ToString());
                voResData.Add("MQTTTopic_2", voobj["MQTTTopic_2"].ToString());
                voResData.Add("MQTTBrokerMultiple", voobj["MQTTBrokerMultiple"].ToString());

                // begin write to json file
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                File.WriteAllText(basepatch, strValue);
                // end wriet to json file

                // publish mqtt
                MqttClient_2.Publish(MQTTTopic_2 + "All/setConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                // Restart Apps
                Process currentProcess = Process.GetCurrentProcess();
                int pid = currentProcess.Id;
                string applicationName = currentProcess.ProcessName;
                RestartApp(pid, applicationName);
                System.Environment.Exit(1);
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        private static void getConfig(string message)
        {
            try
            {
                object result = JsonConvert.DeserializeObject(message);
                JObject voobj = JObject.Parse(result.ToString());

                string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
                if (File.Exists(basepatch))
                {
                    string Json = File.ReadAllText(basepatch);
                    if (Json != "null")
                    {
                        object rootresult = JsonConvert.DeserializeObject(Json);
                        JObject rootobj = JObject.Parse(rootresult.ToString());
                        string mongodb_server = voobj["mongodb_server"].ToString();
                        string signalr_server = voobj["signalr_server"].ToString();
                        string MQTTBroker_1 = voobj["MQTTBroker_1"].ToString();
                        string MQTTBroker_2 = voobj["MQTTBroker_2"].ToString();
                        string MQTTTopic_1 = voobj["MQTTTopic_1"].ToString();
                        string MQTTTopic_2 = voobj["MQTTTopic_2"].ToString();
                        string MQTTBrokerMultiple = voobj["MQTTBrokerMultiple"].ToString();

                        // publish mqtt
                        Dictionary<string, string> voResData = new Dictionary<string, string>();
                        voResData.Add("mongodb_server", mongodb_server);
                        voResData.Add("signalr_server", signalr_server);
                        voResData.Add("MQTTBroker_1", MQTTBroker_1);
                        voResData.Add("MQTTBroker_2", MQTTBroker_2);
                        voResData.Add("MQTTTopic_1", MQTTTopic_1);
                        voResData.Add("MQTTTopic_2", MQTTTopic_2);
                        voResData.Add("MQTTBrokerMultiple", MQTTBrokerMultiple);

                        string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                        MqttClient_2.Publish(MQTTTopic_2 + "All/getConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                    }
                }
            }
            catch (Exception e)
            {
                errorHandler(e.Message);
            }
        }
        protected override void OnStop()
        {
            Dictionary<string, string> voResData = new Dictionary<string, string>();
            voResData.Add("deviceID", "smartwatch");
            voResData.Add("msg", "HASIOT - smartwatch service is stopped");
            string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            MqttClient_2.Publish(MQTTTopic_2 + "All/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
        }

        #region snipet
        static void RestartApp(int pid, string applicationName)
        {
            // Wait for the process to terminate
            Process process = null;
            try
            {
                process = Process.GetProcessById(pid);
                process.WaitForExit(1000);
            }
            catch (ArgumentException ex)
            {
                errorHandler(ex.Message);
            }
            Process.Start(applicationName, "");
        }
        public static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        private static void initRTHub()
        {
            try
            {
                //Set connection
                connection = new HubConnection(hubUrl);
                //Make proxy to hub based on hub name on server
                appHub = connection.CreateHubProxy("apphub");
                //Start connection
                connection.Start().ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        task.Exception.GetBaseException();
                        Console.WriteLine(task.Exception.GetBaseException());
                    }
                    else
                    {
                        Console.WriteLine("Connected");
                    }
                }).Wait();
            }
            catch (Exception ex)
            {
                // SendMail("The service Curtain is reported", "init signalR " + ex.Message);
                // Console.WriteLine("ex init signalR " + ex.Message);
            }
        }

        private static void heandlerSignalR(string mod, string tp, string sp)
        {
            try
            {
                appHub.Invoke<string>("Send", mod, tp, sp).ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        Console.WriteLine("There was an error calling send: {0}", task.Exception.GetBaseException());
                        if (connection.State == Microsoft.AspNet.SignalR.Client.ConnectionState.Disconnected)
                        {
                            initRTHub();
                            heandlerSignalR(mod, tp, sp);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Result Handler signalR " + task.Result);
                    }
                });
            }
            catch (Exception ex)
            {
                //Console.WriteLine("ex Handler signalR " + ex.Message);
                //SendMail("The service Curtain is reported", "init signalR " + ex.Message);
                if (connection.State == Microsoft.AspNet.SignalR.Client.ConnectionState.Disconnected)
                {
                    initRTHub();
                    heandlerSignalR(mod, tp, sp);
                }
            }
        }

        static void errorHandler(string Message)
        {
            // sending error messages
            Dictionary<string, string> voResData = new Dictionary<string, string>();
            voResData.Add("deviceID", "All");
            voResData.Add("msg", Message);
            string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            MqttClient_2.Publish(MQTTTopic_2 + "All/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

            // Restart service
            Process currentProcess = Process.GetCurrentProcess();
            int pid = currentProcess.Id;
            string applicationName = currentProcess.ProcessName;
            RestartApp(pid, applicationName);
            System.Environment.Exit(1);
        }
        #endregion

        private void writeTxt(string log, string logType)
        {
            string newLog = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + log;
            StringBuilder sb = new StringBuilder();
            sb.Append(newLog + Environment.NewLine);
            // flush every 20 seconds as you do it
            File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + logType + "_" + DateTime.Now.ToString("yyyyMMdd") + ".txt", sb.ToString());
            sb.Clear();
        }
    }
}

