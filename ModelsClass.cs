﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace has.iot.smartwatch.service
{
    public class Constants
    {
        public const int SW_ERROR_CODE = 9000;
    }
    public class BLESmartWatchDataView
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public virtual string mac { get; set; }
        public virtual string person { get; set; }
        public virtual string location { get; set; }
        public virtual string type { get; set; }
        public virtual int battery { get; set; }
        public virtual int heartrate { get; set; }
        public virtual int step { get; set; }
        public virtual int sleep { get; set; }
        public virtual int calories { get; set; }
        public virtual int blood { get; set; }
        public virtual decimal temperature { get; set; }
        public virtual bool sos { get; set; }
        public virtual bool unwear { get; set; }
        public virtual bool falling { get; set; }
        public virtual int nearby { get; set; }
        public virtual bool battery_update { get; set; }
        public virtual bool blood_update { get; set; }
        public virtual bool temperature_update { get; set; }
    }
    public class BLESmartWatchData
    {
        public DateTime RecordTimestamp { get; set; }
        public virtual string mac { get; set; }
        public virtual string person { get; set; }
        public virtual string location { get; set; }
        public virtual string type { get; set; }
        public virtual int battery { get; set; }
        public virtual int heartrate { get; set; }
        public virtual int step { get; set; }
        public virtual int sleep { get; set; }
        public virtual int calories { get; set; }
        public virtual int blood { get; set; }
        public virtual decimal temperature { get; set; }
        //public virtual int sos { get; set; }
        //public virtual int unwear { get; set; }
        public virtual bool sos { get; set; }
        public virtual bool unwear { get; set; }
        public virtual bool falling { get; set; }
        public virtual int nearby { get; set; }
        public virtual bool battery_update { get; set; }
        public virtual bool blood_update { get; set; }
        public virtual bool temperature_update { get; set; }
    }
    public class SmartWatchDataLog
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public virtual string mac { get; set; }
        public virtual string person { get; set; }
        public virtual string location { get; set; }
        public virtual string type { get; set; }
        public virtual int battery { get; set; }
        public virtual int heartrate { get; set; }
        public virtual int step { get; set; }
        public virtual int sleep { get; set; }
        public virtual int calories { get; set; }
        public virtual int blood { get; set; }
        public virtual decimal temperature { get; set; }
        //public virtual int sos { get; set; }
        //public virtual int unwear { get; set; }
        public virtual bool sos { get; set; }
        public virtual bool unwear { get; set; }
        public virtual bool falling { get; set; }
        public virtual int nearby { get; set; }
        public virtual bool battery_update { get; set; }
        public virtual bool blood_update { get; set; }
        public virtual bool temperature_update { get; set; }
    }
    public class SmartWatchGatewayView
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public virtual string gatewayID { get; set; }
        public virtual string gatewayName { get; set; }
        public virtual string ipAddress { get; set; }
        public virtual string location { get; set; }
    }

    public class PersonView
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public virtual string personID { get; set; }
        public virtual string personName { get; set; }
        public virtual string defaultZone { get; set; }
        public virtual string avatarID { get; set; }
        public virtual string uuid { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Age { get; set; }
        public virtual string Gender { get; set; }
        public virtual string HealthRemarks { get; set; }
        public virtual string CaseRemarks { get; set; }
        public virtual string PeriodStart { get; set; }
        public virtual string PeriodEnd { get; set; }
        public virtual string DefaultWearable { get; set; }
        public virtual string WearableActivated { get; set; }
        public virtual string SosButton { get; set; }
        public virtual string photo { get; set; }
    }
    public class PersonTagsView
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public virtual string tagID { get; set; }
        public virtual string type { get; set; }
        public virtual string active { get; set; }
        public virtual string personID { get; set; }
    }
    public class PersonLocation
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public virtual string gatewayID { get; set; }
        public virtual string type { get; set; }
        public virtual string tagID { get; set; }
        public virtual string rssi { get; set; }
        public virtual string personID { get; set; }
        public virtual string personName { get; set; }
        public virtual string location { get; set; }
        public virtual string defaultZone { get; set; }
        public virtual string avatarID { get; set; }

    }

    /*Curtain Constant*/
    public class ConfigurationModel
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public decimal OutDoorTemperature { get; set; }

        public bool OutDoorTemperatureStatus { get; set; }

        public decimal ConstantLux { get; set; }

        public bool ConstantLuxStatus { get; set; }

        public decimal SkinTemperature { get; set; }

        public bool SkinTemperatureStatus { get; set; }

        public decimal HeartRate { get; set; }

        public bool HeartRateStatus { get; set; }

        public int HumanTracing { get; set; }

        public bool HumanTracingStatus { get; set; }
        public string ACMVHumanPresence { get; set; }

    }
    public class SmartwatchPersonZone
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public virtual string gatewayID { get; set; }
        public virtual string type { get; set; }
        public virtual string tagID { get; set; }
        public virtual string rssi { get; set; }
        public virtual string personID { get; set; }
        public virtual string personName { get; set; }
        public virtual string location { get; set; }
        public virtual string defaultZone { get; set; }
        public virtual string avatarID { get; set; }
        public virtual string statusPerson { get; set; }
        public DateTime LastSeen { get; set; }

    }

    public class SmartwatchPersonCount
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public virtual string Zone { get; set; }
        public virtual int TotalPeople { get; set; }

    }



    public class SmartWatchDataRoom
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string location { get; set; }
        public int inRoom { get; set; }
    }

    public class SmartWatchDataRoomDetail
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string personID { get; set; }
        public string personName { get; set; }
        public string location { get; set; }
        public string status { get; set; }
        public string avatarID { get; set; }
    }

    public class PersonStatusDatabase
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string personID { get; set; }
        public string personName { get; set; }
        public string status { get; set; }
    }

    public class AvatarLocation
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string avatarName { get; set; }
        public string locationName { get; set; }
        public string location { get; set; }
        public string left { get; set; }
        public string top { get; set; }
    }

    public class EventsLogMonitoring
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public string EventType { get; set; }
        public string TagID { get; set; }
        public string personName { get; set; }
        public string NRIC { get; set; }
        public decimal Temp { get; set; }
        public int HR { get; set; }
        public string Location { get; set; }
        public string Tampered { get; set; }
        public string Acknowledge { get; set; }
        public string ActionRemarks { get; set; }
        public string Status { get; set; }
        public string Geofencing { get; set; }
    }

    public class EventsLogMonitoringTemp
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int Count { get; set; }
        public string EventType { get; set; }
        public string TagID { get; set; }
        public string personName { get; set; }
        public string NRIC { get; set; }
        public decimal Temp { get; set; }
        public int HR { get; set; }
        public string Location { get; set; }
        public string Tampered { get; set; }
        public string Acknowledge { get; set; }
        public string ActionRemarks { get; set; }
        public string Status { get; set; }
        public string Geofencing { get; set; }
    }

    public class HumanConfiguration
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime recordTimestamp { get; set; }
        public string activatedHR { get; set; }
        public int whenBelowHR { get; set; }
        public int whenAboveHR { get; set; }
        public int conPeriodHR { get; set; }
        public int resetAlertHR { get; set; }
        public string activatedTM { get; set; }
        public decimal whenBelowTM { get; set; }
        public decimal whenAboveTM { get; set; }
        public int conPeriodTM { get; set; }
        public int resetAlertTM { get; set; }
        public string activatedSOS { get; set; }
        public int resetAlertSOS { get; set; }
        public string activatedTEM { get; set; }
        public int resetAlertTEM { get; set; }
        public string activatedBattery { get; set; }
        public int conPeriodBattery { get; set; }
        public int resetAlertBattery { get; set; }
        public string activatedGeofencing { get; set; }
        public int conPeriodGeofencing { get; set; }
        public int resetAlertGeofencing { get; set; }

    }
    public class HumanConfigurationTemp
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime recordTimestamp { get; set; }
        public string status { get; set; }
        public string tipe { get; set; }

        public decimal below { get; set; }
        public decimal above { get; set; }
        public int periode { get; set; }
        public int reset { get; set; }
        public string currentStatus { get; set; }
    }
}